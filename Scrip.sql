USE [CrudTrabajoFinal]
GO
/****** Object:  Table [dbo].[Auto]    Script Date: 29-11-2016 19:32:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Auto](
	[au_patente] [varchar](20) NOT NULL,
	[au_marca] [varchar](30) NULL,
	[au_modelo] [varchar](30) NULL,
	[au_color] [varchar](30) NULL,
	[au_ano] [int] NULL,
	[au_kilometraje] [int] NULL,
	[au_precio_lista] [int] NULL,
	[au_estado] [varchar](20) NULL,
 CONSTRAINT [PK_Auto] PRIMARY KEY CLUSTERED 
(
	[au_patente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 29-11-2016 19:32:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cliente](
	[cl_rut] [varchar](20) NOT NULL,
	[cl_nombre] [varchar](30) NULL,
	[cl_apellido_p] [varchar](30) NULL,
	[cl_apellido_m] [varchar](30) NULL,
	[cl_edad] [int] NULL,
 CONSTRAINT [PK_Cliente1] PRIMARY KEY CLUSTERED 
(
	[cl_rut] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Vendedor]    Script Date: 29-11-2016 19:32:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Vendedor](
	[ven_rut] [varchar](20) NOT NULL,
	[ven_nombre] [varchar](30) NULL,
	[ven_apellido] [varchar](30) NULL,
	[ven_fecha_contrato] [varchar](30) NULL,
 CONSTRAINT [PK_Vendedor] PRIMARY KEY CLUSTERED 
(
	[ven_rut] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 29-11-2016 19:32:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Venta](
	[ve_id] [int] IDENTITY(1,1) NOT NULL,
	[ve_precio_net] [int] NULL,
	[ve_precio_iva] [int] NULL,
	[ve_descuento] [int] NULL,
	[ve_precio_final] [int] NULL,
	[ve_patente] [varchar](20) NULL,
	[ve_rut_cliente] [varchar](20) NULL,
	[ve_rut_vendedor] [varchar](20) NULL,
	[ve_fecha] [varchar](20) NULL,
	[ve_hora] [varchar](20) NULL,
 CONSTRAINT [PK_Venta] PRIMARY KEY CLUSTERED 
(
	[ve_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Auto] FOREIGN KEY([ve_patente])
REFERENCES [dbo].[Auto] ([au_patente])
GO
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Auto]
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Cliente] FOREIGN KEY([ve_rut_cliente])
REFERENCES [dbo].[Cliente] ([cl_rut])
GO
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Cliente]
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Venta_Vendedor] FOREIGN KEY([ve_rut_vendedor])
REFERENCES [dbo].[Vendedor] ([ven_rut])
GO
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Venta_Vendedor]
GO
