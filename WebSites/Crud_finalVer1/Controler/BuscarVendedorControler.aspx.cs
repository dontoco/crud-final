﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrudTrabajoFinalModel;

public partial class Controler_BuscarVendedorControler : System.Web.UI.Page
{
    CrudTrabajoFinalEntities BD = new CrudTrabajoFinalEntities();
    protected void Page_Load(object sender, EventArgs e)
    {
        String rut = Request["TxtRut"];

        if (rut.Trim().Equals(""))
        {
            String mensaje = "Debe Ingresar un rut";
            Session["mensaje"] = mensaje;
            Response.Redirect("../Face/confirmaciones/error.aspx");

        }
        else
        {
            try
            {
                var query = (from p in BD.Vendedor
                             let Rut = p.ven_rut
                             let nombre = p.ven_nombre
                             let apellido = p.ven_apellido
                             let FechaContrato = p.ven_fecha_contrato
                             where p.ven_rut == rut
                             select new
                             {
                                 rut,
                                 nombre,
                                 apellido,
                                 FechaContrato
                             });

                MyGrid.DataSource = query.ToList();
                MyGrid.DataBind();
            }
            catch (InvalidOperationException)
            {
                String mensaje = "No se encuentra";
                Session["mensaje"] = mensaje;
                Response.Redirect("../Face/confirmaciones/error.aspx");
            }
        }
    }
}