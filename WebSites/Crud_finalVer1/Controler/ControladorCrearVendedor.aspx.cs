﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrudTrabajoFinalModel;

public partial class Controler_ControladorCrearVendedor : System.Web.UI.Page
{
    CrudTrabajoFinalEntities BD = new CrudTrabajoFinalEntities();
    protected void Page_Load(object sender, EventArgs e)
    {
        String ruven = Request["TxtRut"];
        String noven = Request["TxtNombre"];
        String apeven = Request["TxtApellido"];
        String fcoonven = Request["TxtFeContrato"];

        if (ruven.Trim().Equals("") || noven.Trim().Equals("") || apeven.Trim().Equals("") || fcoonven.Trim().Equals(""))
        {
            String mensaje = "Campos Vacios";
            Session["mensaje"] = mensaje;
            Response.Redirect("../Face/confirmaciones/error.aspx");
        }
        else
        {
            Vendedor v1 = new Vendedor()
            {
                ven_rut = ruven,
                ven_nombre = noven,
                ven_apellido = apeven,
                ven_fecha_contrato = fcoonven
            };

            BD.AddObject("Vendedor", v1);

            try
            {
                BD.SaveChanges();
                String mensaje = "Vendedor Registrado correctamente";
                Session["mensaje"] = mensaje;
                Response.Redirect("../Face/confirmaciones/ok.aspx");
            }
            catch (UpdateException)
            {
                String mensaje = "El Vendedor ya exite en la base de datos ";
                Session["mensaje"] = mensaje;
                Response.Redirect("../Face/confirmaciones/ok.aspx");
            }
                
        }
        
    }
}