﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrudTrabajoFinalModel;

public partial class Controler_ControladorEliminarVendedor : System.Web.UI.Page
{
    CrudTrabajoFinalEntities BD = new CrudTrabajoFinalEntities();

    protected void Page_Load(object sender, EventArgs e)
    {

        String ruven = Request["TxtRut"];


        if (ruven.Trim().Equals("") || ruven == null)
        {
            String mensaje = "Campo vacio";
            Session["mensaje"] = mensaje;
            Response.Redirect("../Face/confirmaciones/error.aspx");
        }
        else
        { 
        try
        {
            

            Vendedor v1 = (from p in BD.Vendedor
                           where p.ven_rut == ruven
                           select p).First();

            BD.DeleteObject(v1);
            BD.SaveChanges();

            String mensaje = "Vendedor Eliminado Correctamente";
            Session["mensaje"] = mensaje;
            Response.Redirect("../Face/confirmaciones/ok.aspx");

            


        }
        catch (InvalidOperationException)
        {
            String mensaje = "Vendedor no existe";
            Session["mensaje"] = mensaje;
            Response.Redirect("../Face/confirmaciones/ok.aspx");

            }

        }

    }
}