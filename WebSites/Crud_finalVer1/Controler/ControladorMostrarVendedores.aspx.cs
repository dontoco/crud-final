﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrudTrabajoFinalModel;

public partial class Controler_ControladorMostrarVendedores : System.Web.UI.Page
{
    CrudTrabajoFinalEntities BD = new CrudTrabajoFinalEntities();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            var query = (from v in BD.Vendedor
                         let Rut = v.ven_rut
                         let Nombre = v.ven_nombre
                         let Apellido = v.ven_apellido
                         let F_contrato = v.ven_fecha_contrato
                         select new
                         {
                             Rut,
                             Nombre,
                             Apellido,
                             F_contrato
                         });
            gvVendedores.DataSource = query.ToList();
            gvVendedores.DataBind();
        }
        catch (InvalidOperationException)
        {
            String mensaje = "No Hay Registros";
            Session["mensaje"] = mensaje;
            Response.Redirect("../Face/confirmaciones/error.aspx");
        }
    }
}