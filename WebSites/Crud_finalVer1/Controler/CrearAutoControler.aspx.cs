﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrudTrabajoFinalModel;
using System.Data.SqlClient;
using System.Data;


public partial class Controler_CrearAutoControler : System.Web.UI.Page
{
    CrudTrabajoFinalEntities BD = new CrudTrabajoFinalEntities();
    protected void Page_Load(object sender, EventArgs e)
    {
        String pate = Request["TxtPatente"];
        String marc = Request["TxtMarca"];
        String mod = Request["TxtModelo"];
        String colo = Request["TxtColor"];
        String ano = Request["TxtAno"]; // iont
        String kilo = Request["TxtKilometro"]; //int
        String plista = Request["TxtPlista"];  // int 
        String estad = Request["TxtEstado"];

        if (pate.Trim().Equals("") || kilo.Trim().Equals("") || marc.Trim().Equals("") ||mod.Trim().Equals("") || colo.Trim().Equals("") || ano.Trim().Equals("") || plista.Trim().Equals("") || estad.Trim().Equals("") )
        {
            String mensaje = "Campo vacio";
            Session["mensaje"] = mensaje;
            Response.Redirect("../Face/confirmaciones/error.aspx");
        }
        else
        {   
            int an = 0; // parce de año 
            int ki = 0; // parce de kilometro
            int pl = 0; // parce de Precio lista 
            
            try
            {
                an = Int32.Parse(ano);
                ki = Int32.Parse(kilo);
                pl = Int32.Parse(plista);
            }
            catch (FormatException)
            {
                String mensaje = "Estos campos deben ser numericos ";
                Session["mensaje"] = mensaje;
                Response.Redirect("../Face/confirmaciones/error.aspx");
            }

            Auto a1 = new Auto
            {
                au_patente = pate,
                au_marca = marc,
                au_modelo = mod,
                au_color = colo,
                au_ano = an,
                au_kilometraje = ki,
                au_precio_lista = pl,
                au_estado = estad
            };
            BD.AddObject("Auto", a1);
            try
            {
                BD.SaveChanges();
                String mensaje = "Automovil  Registrado correctamente";
                Session["mensaje"] = mensaje;
                Response.Redirect("../Face/confirmaciones/ok.aspx");


                
            }
            catch (UpdateException)
            {
                String mensaje = "El Auto  ya exite en la base de datos WEON!!!!! ";
                Session["mensaje"] = mensaje;
                Response.Redirect("../Face/confirmaciones/ok.aspx");
            }


        }
    }
}
