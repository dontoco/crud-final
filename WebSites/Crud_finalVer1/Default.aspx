﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="css/StyleSheet.css" />
    <style type="text/css">
        .style1
        {
            font-size: medium;
        }
    </style>
</head>
<body style="height: 771px">
    <form id="form1" runat="server">
    <div align="center">
     <h1 align="center">
            Mantenedor Registro y compra de Autos
        </h1>
        <table border="0" cellpadding = "0" cellspacing = "0">
        <thead>
            <tr>
                <th>Panel de acciones</th>
            </tr>
            </thead>
    
       
        <h3>Vendedor</h3>
        <table>
        <tr>
        <td>
    
        <asp:Button ID="Button1" runat="server" PostBackUrl="~/Face/CrearVendedor.aspx" 
            Text="Crear Vendedor" Width="167px" />
         </td>
         <td>                    
    
        <asp:Button ID="Button2" runat="server" 
            PostBackUrl="~/Face/EliminarVendedor.aspx" Text="Borar Vendedor" 
            Width="167px" />
        </td>    
      
        <td>
        <asp:Button ID="Button3" runat="server" Text="Listar Vendedor" 
            PostBackUrl="~/Face/MostrarVendedores.aspx" Width="168px" />
        </td>
        <td>
        <asp:Button ID="Button4" runat="server" Text="Modificar Vendedor" />
        </td>
        <td>
          
        <asp:Button ID="Button5" runat="server" 
            PostBackUrl="~/Face/buscarVendedor.aspx" Text="Buscar Vendedor" 
            Width="169px" />
            </td>
            
        </tr>
        </table>
       <h3>Autos</h3> 
       <table>
       <tr>
       <td>
        <asp:Button ID="Button6" runat="server" Text="Crear Auto" Width="174px" 
            style="text-align: center" PostBackUrl="~/Face/CrearAutos.aspx" />
            </td>
       <td>
        <asp:Button ID="Button7" runat="server" Text="Borrar Auto" Width="174px" />
        </td>
        <td>
        <asp:Button ID="Button8" runat="server" Text="Listar Auto" Width="173px" />
        </td>
         <td>
        <asp:Button ID="Button9" runat="server" Text="Modificar Auto" Width="171px" />
        </td>
          <td>
        <asp:Button ID="Button10" runat="server" Text="Buscar Auto" Width="170px" />
         </td>
        </tr>
        </table>
        <h3>Venta</h3>
        <table>
        <tr>
        <td>
        <asp:Button ID="Button11" runat="server" Text="Nueva Venta" Width="171px" />
        </td>
         <td>
        <asp:Button ID="Button12" runat="server" Text="Eliminar Venta" Width="169px" />
          </td>
      <td>
        <asp:Button ID="Button13" runat="server" Text="Listar Venta" Width="167px" />
       </td>
        <td>
        <asp:Button ID="Button14" runat="server" Text="Modificar Venta" Width="167px" />
        </td>
        <td>
        <asp:Button ID="Button15" runat="server" Text="Imprimir Venta" Width="168px" />
        </td>
        
        </tr>
       </table>
    
    </div>
    </form>
</body>
</html>
