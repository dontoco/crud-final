﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CrearAutos.aspx.cs" Inherits="Face_CrearAutos" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../css/StyleSheet.css" />
</head>
<body style="height: 363px">
    <form id="form1" runat="server">
    <div align="center">
        <h1 align="center">Registro de Autos
        </h1>
        <hr />
        <br />
    
        <table border= "0" cellpadding ="0" cellspacing="0">
       <thead>
        <tr>
            <td>Insertar nuevo Auto</td>
        </tr>
       </thead>
        <tbody>
            <tr>
                <td>Patente</td>
                <td>
                    <asp:TextBox ID="TxtPatente" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>Marca</td>
                <td>
                    <asp:TextBox ID="TxtMarca" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>Modelo</td>
                <td>
                    <asp:TextBox ID="TxtModelo" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>Color</td>
                <td>
                    <asp:TextBox ID="TxtColor" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
              <tr>
                <td>Año</td>
                <td>
                    <asp:TextBox ID="txtAno" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
              <tr>
                <td>Kilometraje</td>
                <td>
                    <asp:TextBox ID="TxtKilometro" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
              <tr>
                <td>Precio</td>
                <td>
                    <asp:TextBox ID="TxtPlista" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
              <tr>
                <td>Estado</td>
                <td>
                    <asp:TextBox ID="TxtEstado" runat="server"></asp:TextBox>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                 
                    <asp:Button ID="btnGuardar3" runat="server" Text="Guardar" 
                        PostBackUrl="~/Controler/CrearAutoControler.aspx" /> 
               </td>
            </tr>
        </tfoot>
       </table>
    
    </div>
    </form>
</body>
</html>
