﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CrearVendedor.aspx.cs" Inherits="Face_CrearVendedor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../css/StyleSheet.css"/>
   
</head>
<body>
    <form id="form1" runat="server">
   <div align ="center">
        <h1 align ="center">
            Registro de Vendedores
       </h1>
       <hr />
       <br />
       <table border= "0" cellpadding ="0" cellspacing="0">
       <thead>
        <tr>
            <td>Insertar nuevo Vendedor</td>
        </tr>
       </thead>
        <tbody>
            <tr>
                <td>Rut</td>
                <td>
                    <asp:TextBox ID="TxtRut" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>Nombre</td>
                <td>
                    <asp:TextBox ID="TxtNombre" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
             <tr>
                <td>Apellidos</td>
                <td>
                    <asp:TextBox ID="TxtApellido" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
             
             <tr>
                <td>Fecha de Contrato</td>
                <td>
                    <asp:TextBox ID="TxtFeContrato" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
        </tbody>
        <tfoot>
            <tr>
                <td>
                   <asp:Button ID="Button1" runat="server" Text="Crear" 
                        PostBackUrl="~/Controler/ControladorCrearVendedor.aspx" /> 
                      
                       
               </td>
            </tr>
        </tfoot>
       </table>
    
    </div>
    </form>
</body>
</html>
