﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EliminarVendedor.aspx.cs" Inherits="Face_EliminarVendedor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="Stylesheet" href="../css/StyleSheet.css" />
    <style type="text/css">
        .style1
        {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
      <div align ="center">
        <h1 align ="center">
            Eliminar registro de Vendedor
       </h1>
       <hr />
       <br />
       <table border= "0" cellpadding ="0" cellspacing="0">
       <thead>
        <tr>
            <td>Ingrese el RUT  del vendedor</td>
        </tr>
       </thead>
        <tbody>
            <tr>
                <td>Rut</td>
                <td>
                    <asp:TextBox ID="txtRut1" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            
        </tbody>
        <tfoot>
            
            <tr>
                <td>
                    <asp:Button ID="BtnEliminar" runat="server" Text="Eliminar Vendedor" 
                        PostBackUrl="~/Controler/ControladorEliminarVendedor.aspx" />
                                             
               </td>
            </tr>
        </tfoot>
       </table>
    
    </div>
    </form>
</body>
</html>


