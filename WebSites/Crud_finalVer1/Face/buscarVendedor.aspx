﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="buscarVendedor.aspx.cs" Inherits="Face_buscarVendedor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <link rel="Stylesheet" href="../css/StyleSheet.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div align ="center">
        <h1 align ="center">
            Buscar Vendedor
       </h1>
       <hr />
       <br />
       <table border= "0" cellpadding ="0" cellspacing="0">
       <thead>
        <tr>
            <td>Ingrese el RUT  del vendedor</td>
        </tr>
       </thead>
        <tbody>
            <tr>
                <td>Rut</td>
                <td>
                    <asp:TextBox ID="txtRut1" runat="server" MaxLength="20"></asp:TextBox>
                </td>
            </tr>
            
        </tbody>
        <tfoot>
            
            <tr>
                <td>
                    <asp:Button ID="BtnBuscar" runat="server" Text="Buscar Vendedor" 
                        PostBackUrl="~/Controler/BuscarVendedorControler.aspx" />
                                             
               </td>
            </tr>
        </tfoot>
       </table>
    
    </div>
    </form>
</body>
</html>


